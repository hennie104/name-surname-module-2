main(List<String> args) {
  var winners = [
    'FNB Banking',
    'Bookly',
    'Live-inspect',
    'CPUT-mobile',
    'Domestly',
    'OrderIn',
    'Cowa-bunga',
    'Digger',
    'Checkers Sixty',
    'Ambani Africa'
  ];

  winners.sort();
  print('The winners in alphabetical order: $winners');

  print('The winner for 2017 was ' + winners[6]);
  print('The winner for 2018 was ' + winners[9]);

  print(
      'There have been a total of ' + (winners.length).toString() + ' winners');
}
