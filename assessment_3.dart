void main() {
  var appOne = appInfo();
  appOne.name = 'Takealot App';
  appOne.category = 'Best Consumer Solution';
  appOne.developer = 'Danny Wang';
  appOne.year = 2021;

  appOne.output();

  appOne.upperCase();
}

class appInfo {
  String name = '';
  var category;
  var developer;
  var year;

  void output() {
    print(
        'App Name: ${this.name}\nCategory: ${this.category}\nDeveloper: ${this.developer}\nYear: ${this.year}');
  }

  void upperCase() {
    print(this.name.toUpperCase());
  }
}
